package com.example.sessionmanagement;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoggedActivity extends AppCompatActivity {
    SharedPreferences pref;
    Intent intentL;
    TextView txtUsername;
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);

        txtUsername = findViewById(R.id.txtUsername);
        btnLogout = findViewById(R.id.btnLogout);

        //setting a name to the shared preferences storage (the same used in the MainActivity.class
        pref = getSharedPreferences("loginInformation", MODE_PRIVATE);

        //setting possible intent (other activities/pages that the app can show)
        intentL = new Intent(LoggedActivity.this, MainActivity.class);

        //getting the username of the user from shared preferences and key value: userName
        String welcome = pref.getString("userName", "");

        txtUsername.setText("Hi, " + welcome);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the user clicks on logout, clear the shared preferences
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();

                //goes to MainActivity.class page
                startActivity(intentL);
            }
        });
    }
}
