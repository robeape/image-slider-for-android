package com.example.sessionmanagement;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class RegisterActivity extends AppCompatActivity {
    FileOutputStream fStreamO;
    Intent intentRA;
    EditText newUser, newPassword;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        newUser = findViewById(R.id.edtNewUsername);
        newPassword = findViewById(R.id.edtNewPassword);
        btnSave = findViewById(R.id.btnSave);
        intentRA = new Intent(RegisterActivity.this, MainActivity.class);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = newUser.getText().toString()+"\n";
                String password = newPassword.getText().toString();
                try{
                    //if the user tries to register, save his information on an internal storage named "registerDetails"
                    fStreamO = openFileOutput("registerDetails", Context.MODE_PRIVATE);
                    //insert his name and password
                    fStreamO.write(username.getBytes());
                    fStreamO.write(password.getBytes());
                    fStreamO.close();
                    Toast.makeText(getApplicationContext(), "User details saved successfully", Toast.LENGTH_LONG).show();
                    //once the system has already saved everything, goes to the login page (MainActivity.class)
                    startActivity(intentRA);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
